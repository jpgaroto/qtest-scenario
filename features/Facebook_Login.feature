Feature: Facebook Login
  As a user i can log into the system via Facebook
  
  Scenario: Sucess Facebook login
    Given a web browser is on the Login page
    And the user has already registered
    When the username is entered
    And the password in entered
    And the Login button is clicked
    Then the Welcome page is shown